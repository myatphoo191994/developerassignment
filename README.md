# WebDeveloperAssignment 



### Server Requirements

You will need to make sure your server meets the following requirements:
* Nginx >= 1.10
* PHP >= 7.3
* MySQL >= 5.7
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension

### Installation

#### Git Clone
`$ git clone https://myatphoo191994@bitbucket.org/myatphoo191994/developerassignment.git`

#### Composer
Laravel project dependencies are managed through the PHP Composer tool. The first step is to install the depencencies by navigating into your project in terminal and typing this command: <br/>

`$ composer install`

You may be getting outdated dependencies. Run update to update them.

`$ composer update`

#### NPM
In order to install the Javascript packages for frontend development, you will need the Node Package Manager , If you only have NPM installed you have to run this command from the root of the project:<br/>

`$ npm install`

#### Environment Files
This folder ships with a .env.example file in the root of the project.
You must copy and rename this file to just .env <br/>
Note: Make sure you have hidden files shown on your system.

#### Create Database
You must create your database on your server and on your .env file update the following lines:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=assignment
DB_USERNAME=root
DB_PASSWORD=root 
```

Change these lines to reflect your new database settings.

#### Artisan Commands
The first thing we are going to so is set the key that Laravel will use when doing encryption.

`$ php artisan key:generate`

You should see a green message stating your key was successfully generated. As well as you should see the APP_KEY variable in your .env file reflected.

It's time to see if your database credentials are correct.

We are going to run the built-in migrations to create the database tables:

`$ php artisan migrate`

You should see a message for each table migrated, if you don't and see errors, than your credentials are most likely not correct.

We are now going to set the administrator account information.

Now seed the database with:

`$ php artisan db:seed`

You should get a message for each file seeded, you should see the information in your database tables.

By default, the public disk uses the local driver and stores these files in storage/app/public. To make them accessible from the web, you should create a symbolic link from public/storage to
storage/app/public. To create the symbolic link, you may use the storage:link Artisan command:

`$ php artisan storage:link`

#### Directory Permissions

After installing, you may need to configure some permissions. Directories within the **storage** and the **bootstrap/cache** directories should be writable by your web server or Laravel will not run.

#### Running Mix

Mix is a configuration layer on top of Webpack, so to run your Mix tasks you only need to execute one of the NPM scripts that is included with the default Laravel package.json file:

`$ npm run dev` //Run all Mix tasks

`$ npm run production` //Run all Mix tasks and minify output

### Features

* Custom Access Control System (Authentication/Users)
    * Register/Login/Logout/Password Reset
    * Login/Register Throttling
* Book Data Management
  * Book data CRUD
  * Sort by title or author
  * Search for a book by title or author
  * Export with CSV/XML with selected data only  
* Administration Dashboard with  Theme
* Namespaced Routes
* Form/HTML Facades Included
* Master Layout Files with common sections
* Javascript/jQuery Snippets
* Bootstrap 3
* Font Awesome
* jQuery Datatables
* Standards
    * Clean Controllers
    * Repository/Contract Implementations
    * Request Classes
    * Events/Handlers

## Authors

- [Myat Phu][link-author]
