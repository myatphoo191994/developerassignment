<?php

namespace App\Http\Controllers\Book;

use App\Http\Requests\Book\ManageBookRequest;
use App\Http\Requests\Book\StoreBookRequest;
use App\Http\Requests\Book\UpdateBookRequest;
use App\Models\Book\Book;
use App\Repositories\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\View\View;

/**
 * Class BookController
 * @package App\Http\Controllers\Book
 */
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    // space that we can use the repository from
    protected $book;

    /**
     * BookController constructor.
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        // set the model
        $this->book = new Repository($book);
    }

    /**
     * @param ManageBookRequest $request
     * @return Application|Factory|View
     */
    public function index(ManageBookRequest $request)
    {
        return view('book.index');
    }

    /**
     * @param ManageBookRequest $request
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function create(ManageBookRequest $request)
    {
        return view('book.create');
    }

    /**
     * @param StoreBookRequest $request
     * @return mixed
     */
    public function store(StoreBookRequest $request)
    {
        //create record and pass in only fields that are fillable
        $save = $this->book->create($request->only($this->book->getModel()->fillable));

        if ($save) {
            return redirect()->route('book.index')->withFlashSuccess('This book data was successfully created.');
        }
    }

    /**
     * @param Book $book
     * @param ManageBookRequest $request
     * @return Application|Factory|View
     */
    public function edit(Book $book, ManageBookRequest $request)
    {
        return view('book.edit', compact('book'));
    }

    /**
     * @param Book $book
     * @param UpdateBookRequest $request
     * @return mixed
     */
    public function update(Book $book, UpdateBookRequest $request)
    {
        // update model and only pass in the fillable fields
        $save = $this->book->update($request->only($this->book->getModel()->fillable), $book->id);

        if ($save) {
            return redirect()->route('book.index')->withFlashSuccess('This book data was successfully updated.');
        }
    }

    /**
     * @param Book $book
     * @param ManageBookRequest $request
     * @return mixed
     */
    public function deleteBook(Book $book, ManageBookRequest $request)
    {
        $this->book->delete($book->id);
        return redirect()->route('book.index')->withFlashSuccess('This book data was successfully deleted');
    }
}
