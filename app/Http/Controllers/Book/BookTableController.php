<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use App\Http\Requests\Book\ManageBookRequest;
use App\Repositories\Book\BookRepository;
use Yajra\DataTables\Facades\DataTables;

/**
 * Class BookTableController
 * @package App\Http\Controllers\Book
 */
class BookTableController extends Controller
{
    protected $book;

    /**
     * BookTableController constructor.
     * @param BookRepository $book
     */
    public function __construct(BookRepository $book)
    {
        $this->book = $book;
    }

    /**
     * @param ManageBookRequest $request
     * @return mixed
     * @throws \Exception
     */
    public function __invoke(ManageBookRequest $request)
    {
        // return book data using DataTable
        return Datatables::make($this->book->getForDataTable())
            ->escapeColumns(['title'])
            ->addIndexColumn()
            ->addColumn('actions', function ($book) {
                return $book->action_buttons;
            })
            ->make(true);
    }
}
