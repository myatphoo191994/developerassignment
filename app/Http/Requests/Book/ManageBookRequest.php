<?php

namespace App\Http\Requests\Book;

use App\Http\Requests\Request;

/**
 * Class ManageBookRequest
 * @package App\Http\Requests\Book
 */
class ManageBookRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }
}
