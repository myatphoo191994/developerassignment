<?php

namespace App\Http\Requests\Book;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateBookRequest
 * @package App\Http\Requests\Book
 */
class UpdateBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *s
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'author' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'Please enter the :attribute of the book.',
        ];
    }

    /**
     * Get custom attribute's label for validator errors.
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
        ];
    }
}
