<?php

namespace App\Models\Book\Attribute;

/**
 * Trait BookAttribute
 * @package App\Models\Book\Attribute
 */
trait BookAttribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
            return '<a href="' . route('book.edit', $this) . '" class="btn btn-xs btn-primary">Edit</a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="' . route('book.delete', $this) . '" class="btn btn-xs btn-danger">Delete</a>';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
            $this->getEditButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }

}
