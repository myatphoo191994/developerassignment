<?php

namespace App\Models\Book;

use App\Models\Book\Attribute\BookAttribute;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * @package App\Models\Book
 */
class Book extends Model
{
    use BookAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'book';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'author',
    ];

}
