<?php

namespace App\Repositories\Book;

use App\Models\Book\Book;

/**
 * Class ContactRepository
 * @package App\Repositories\Book
 */
class BookRepository
{
    /**
     * Associated Book Model.
     */
    const MODEL = Book::class;

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()->orderBy('created_at', 'desc');
    }

    public function query()
    {
        return call_user_func(static::MODEL . '::query');
    }
}
