<?php namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

/**
 * Class Repository
 * @package App\Repositories
 */
class Repository implements RepositoryInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->all();
    }


    // create a new record in the database
    public function create(array $data)
    {
        if (array_key_exists("password", $data)) {
            $data['password'] = Hash::make($data['password'], ['cost' => 13]);
        }

        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        //check password is null or not
        $record = $this->model->find($id);

        if (array_key_exists("password", $data)) {
            if ( $data['password'] == null ) {

                //user not input password
                $data['password'] = $record->password;

            }else{
                //user input password
                // $data['password'] = bcrypt($data['password']);
                $data['password'] = Hash::make($data['password'], ['cost' => 13]);
            }
        }

        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}
