<?php
namespace Database\Seeders;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class BookTableSeeder
 */
class BookTableSeeder extends Seeder
{
    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        //Add the book data, start from id 1
        $books = [
            [
                'title' => 'Pride and Prejudice',
                'author' => 'Jane Austen',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title' => 'Alice\'s Adventures in Wonderland',
                'author' => 'Lewis Carrol',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title' => 'Adventures of Tow Sawyer',
                'author' => 'Mark Twain',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

        ];

        DB::table('book')->insert($books);
    }
}
