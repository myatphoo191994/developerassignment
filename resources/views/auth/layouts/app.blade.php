<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@yield('meta_description', 'Web Developer Assignment')">
    <meta name="author" content="@yield('meta_author', 'Myat Phu')">
    <title>Web Developer Assignment</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Developer Assignment') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Template design CSS -->
    <!-- Icons -->
    {{ Html::style("css/nucleo.css") }}
    {{ Html::style("css/all.min.css") }}
    {{ Html::style("css/argon.css?v=1.2.0") }}
    <!-- End of Template design CSS -->

</head>
<body class="bg-default">

@include('auth.layouts.nav')
<!-- Main content -->
<div class="main-content">

    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                        <h1 class="text-white">Welcome!</h1>
                        <p class="text-lead text-white">This is the web developer assignment.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                 xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>

    <!-- Page content -->
    @yield('content')

</div>

@include('auth.layouts.footer')

<!-- Template Scripts -->
<!-- Core -->
{{ Html::script("js/jquery.min.js") }}
{{ Html::script("js/bootstrap.bundle.min.js") }}
{{ Html::script("js/js.cookie.js") }}
{{ Html::script("js/jquery.scrollbar.min.js") }}
{{ Html::script("js/jquery-scrollLock.min.js") }}

<!-- Argon JS -->
{{ Html::script("js/argon.js?v=1.2.0") }}
<!-- End of Template Scripts -->

</body>
</html>
