@extends ('book.layouts.app')

@section('content')

    <div class="container-fluid mt--6">
        <div class="row mt--5">
            <div class="col-md-10 ml-auto mr-auto">
                <div class="card card-upgrade">
                    <div class="card-header text-center border-bottom-0">
                        <h4 class="card-title">Create New Book</h4>
                        <p class="card-category"></p>
                    </div>
                    @include('book.layouts.messages')

                    <div class="card-body">

                        {{ Form::open(['route' => 'book.store', 'class' => 'form-horizontal','id' => 'phone-form','method' => 'post']) }}

                        <div class="form-group">
                            {{ Form::label('title', 'Book Title *', ['class' => 'control-label']) }}

                            {{ Form::text('title', null, ['name' => 'title','class' => 'form-control', 'required'=> true]) }}
                            @if($errors->has('title'))
                                <span
                                    class="help-block error-help-block server-validation-errors">{{ $errors->first('title') }}</span>
                            @endif
                        </div><!--form control-->

                        <div class="form-group">
                            {{ Form::label('author', 'Author *', ['class' => 'control-label']) }}

                            {{ Form::text('author', null, ['name' => 'author','class' => 'form-control', 'required'=> true]) }}
                            @if($errors->has('author'))
                                <span
                                    class="help-block error-help-block server-validation-errors">{{ $errors->first('author') }}</span>
                            @endif
                        </div><!--form control-->

                        <div class="btn-wrapper">
                            {{ Form::submit('Create', ['class' => 'btn btn-primary ']) }}
                            <div class="text-right">
                                {{ link_to_route('book.index', 'Cancel', [], ['class' => 'btn btn-danger']) }}
                            </div>
                        </div>

                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
