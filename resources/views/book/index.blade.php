@extends ('book.layouts.app')

@section('after-styles')
    {{--    dataTable StyleSheet --}}
    {{ Html::style("css/datatables/dataTables.bootstrap.min.css") }}
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
@endsection

@section('content')

    <div class="container-fluid mt--6">
        <div class="row mt--5">
            <div class="col-md-10 ml-auto mr-auto">
                <div class="card card-upgrade">
                    <div class="card-header text-center border-bottom-0">
                        <h4 class="card-title">Book Data List</h4>
                        <p class="card-category"></p>
                    </div>
                    @include('book.layouts.messages')

                    <div class="card-body">
                        <div class="table-responsive table-upgrade">

                            <div class="table-responsive">
                                <table id="book-table" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after-scripts')

    {{ Html::script("js/datatables/jquery.dataTables.min.js") }}
    {{ Html::script("js/datatables/dataTables.bootstrap.min.js") }}

    {{ Html::script("js/datatables/dataTables.buttons.min.js") }}
    {{ Html::script("js/datatables/jszip.min.js") }}
    {{ Html::script("js/datatables/buttons.html5.min.js") }}

    <script src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.colVis.js"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function () {
            $('#book-table').DataTable({
                processing: true,
                serverSide: false,
                pageLength: 25,
                lengthMenu: [25, 50, 100, 500],
                ajax: {
                    url: '{{ route("book-list") }}',
                    type: 'get',
                    data: {trashed: false}
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex' ,searchable: false},
                    {data: 'title', name: 'title'},
                    {data: 'author', name: 'author'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                dom: 'Blfrtip',
                buttons: [
                    {
                        text: 'Export CSV',
                        extend: 'csvHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    {
                        text: 'Export XML',
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible'
                        }
                    },
                    'colvis'
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                initComplete: function () {
                    this.api().columns([1,2]).every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                            .on('keyup', function () {
                                column.search($(this).val()).draw();
                            });
                    });
                }
            });
        });

    </script>

@endsection
