<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Developer Assignment') }}</title>
    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Web Developer Assignment')">
    <meta name="author" content="@yield('meta_author', 'Myat Phu')">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    @yield('meta')

    <!-- Styles -->
    @yield('before-styles')

    <!-- Template design CSS -->
    <!-- Icons -->
    {{ Html::style("css/nucleo.css") }}
    {{ Html::style("css/all.min.css") }}
    {{ Html::style("css/argon.css?v=1.2.0") }}
    <!-- End of Template design CSS -->

    @yield('after-styles')

</head>
<body>

@include('book.layouts.sidebar')
<!-- Main content -->
<div class="main-content" id="panel">
    <!-- Top header -->
    @include('book.layouts.header')

    <!-- header menu -->
    <div class="header bg-primary pb-6">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 text-white d-inline-block mb-0">Book Management</h6>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="{{ route('book.create') }}" class="btn btn-neutral">Create New Book</a>
                        <a href="{{ route('book.index') }}" class="btn btn-neutral">All Book</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Page content -->
@yield('content')

</div>

@yield('before-scripts')

<!-- Template Scripts -->
<!-- Core -->
{{ Html::script("js/jquery.min.js") }}
{{ Html::script("js/bootstrap.bundle.min.js") }}
{{ Html::script("js/js.cookie.js") }}
{{ Html::script("js/jquery.scrollbar.min.js") }}
{{ Html::script("js/jquery-scrollLock.min.js") }}

<!-- Argon JS -->
{{ Html::script("js/argon.js?v=1.2.0") }}
<!-- End of Template Scripts -->

@yield('after-scripts')
</body>
</html>
