<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\Book\BookController::class, 'index'])->name('home');

/*
 * Route for Book
 */
Route::group(['namespace' => 'App\Http\Controllers\Book', 'middleware' => ['auth']], function () {

    /*
     * For DataTables
     */
    Route::name('book-list')->get('book-list', 'BookTableController');

    /*
     * Book CRUD
     */
    Route::resource('book', 'BookController', [
        'except' => ['show', 'delete']
    ]);

    Route::get('bookdelete/{book}', 'BookController@deleteBook')->name('book.delete');

});

